const path = require('path')
const fs = require('fs').promises
const mkdirp = require('mkdirp')
const pkg = require('package-json')

const packages = [
  { name: '@gitlab/noop', versions: ['1.0.0'] },
  { name: '@leipert/bundle-dependencies', versions: ['1.0.0', '1.0.1', '1.0.2'] },
  { name: 'fsevents', versions: ['2.3.2'] },
  { name: 'jackspeak', versions: ['2.3.5'] },
  { name: 'jsonfile', versions: ['4.0.0'] },
  { name: 'keyv', versions: ['3.1.0'] },
  { name: 'lodash', versions: ['4.17.20'] },
  { name: 'ms', versions: ['2.1.3'] },
  { name: 'node-gyp', versions: ['9.0.0'] }
]

async function main () {
  for (const { name, versions } of packages) {
    console.log(`Updating mocks for ${name}`)

    const { versions: allVersions, ...metadata } = await pkg(name, { allVersions: true })

    const filteredVersions = Object.fromEntries(versions.map(v => [v, allVersions[v]]))

    const document = JSON.stringify({ ...metadata, versions: filteredVersions }, null, 2)

    const targetPath = path.join(__dirname, 'package-mocks', `${name}.json`)

    await mkdirp(path.dirname(targetPath))

    await fs.writeFile(targetPath, document)
  }
}

main().then(() => {
  console.log('Success!')
})
