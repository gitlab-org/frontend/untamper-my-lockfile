# mock-registry

In order to not be affected by potential woes of the npm registry, this folder
contains a simple mock-registry to be used in tests.

The mock registry uses real package data which can be found under [package-mocks](./package-mocks).

## Usage

```js
const { setupMockRegistry, clearMockRegistry } = require('../../registry/setup-registry')

test.before(() => { setupMockRegistry() })
test.after(() => { clearMockRegistry() })
```

## Adding a new package (or package version)

1. Simply add the package / version you want to have to [./update-package-mocks](./update-package-mocks.js)
1. Run:

    ```bash
    node mocks/registry/update-package-mocks.js
    ```
