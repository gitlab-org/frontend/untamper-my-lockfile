const npa = require('npm-package-arg')

// Capture the name as the very beginning until we hit an "@" (which we didn't start with)
// note: "?" makes ".+" non-greedy so we don't include extra "@"s
const PACKAGE_NAME_V2_REGEX = /^(?<name>@?.+?)@(?<scheme>.+?):(?<locator>.*)/

function parsePackageName (pattern) {
  const { name, type, subSpec } = npa(pattern)
  if (type === 'alias') {
    return parsePackageName(subSpec.raw)
  }
  return name
}

function getPackageAlias (pattern) {
  const { type, rawSpec } = npa(pattern)
  if (type === 'alias') {
    return rawSpec
  }
  return null
}

/**
 * Handles the YarnV2 use case of `lodash@npm:1.2.3` where it looks like an alias
 * but is missing the `lodash@npm:lodash@1.2.3` part.
 *
 * @param {string} pattern
 * @returns {string}
 */
function parsePackageNameV2 (pattern) {
  const match = PACKAGE_NAME_V2_REGEX.exec(pattern)

  if (!match) {
    throw new Error(`Found unepxected pattern for YarnV2 resolution while parsing package name: ${pattern}`)
  }

  const { name, locator } = match.groups

  const locatorAtIndex = locator.indexOf('@')

  // If the locator was something like `jquery@2` (for example, `jquery2@npm:jquery@2`)
  if (locatorAtIndex >= 0) {
    const packageName = locator.substring(0, locatorAtIndex)

    return packageName
  }

  // If the locator just contained the version with no aliasing
  return name
}

module.exports = {
  getPackageAlias,
  parsePackageName,
  parsePackageNameV2
}
