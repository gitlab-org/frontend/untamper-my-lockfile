/**
 * This function receives a YarnV2 entry and returns a YarnV1 style set of
 * dependencies (that is, including `optionalDependencies`)
 *
 * @param {{ dependencies: Record<string, string>,
 *           dependenciesMeta: Record<string, any> }} yarnEntry
 * @return {{ dependencies: Record<string, string>,
 *            optionalDependencies: Record<string, string> }}
 */
function readV2DependenciesAsV1 ({ dependencies = {}, dependenciesMeta = {} }) {
  return Object.entries(dependencies)
    .reduce((acc, [key, value]) => {
      const isOptional = dependenciesMeta[key]?.optional

      if (isOptional) {
        acc.optionalDependencies[key] = value
      } else {
        acc.dependencies[key] = value
      }

      return acc
    }, { dependencies: {}, optionalDependencies: {} })
}

module.exports = {
  readV2DependenciesAsV1
}
