const fs = require('fs/promises')
const { dirname, join } = require('path')
const { logger } = require('../utils')

async function tryLoadingPackageJSON (filePath) {
  try {
    return JSON.parse(await fs.readFile(join(dirname(filePath), 'package.json'), 'utf8'))
  } catch (e) {
    logger.log('Note: Associated package.json not readable, cannot do advanced resolution checks')
  }
  return {}
}

module.exports = {
  tryLoadingPackageJSON
}
