const test = require('ava')

const { parsePackageNameV2, getPackageAlias } = require('./parse-package-name')

function testParsePackageNameV2 (t, a, b) {
  t.is(parsePackageNameV2(a), b)
}
testParsePackageNameV2.title = (providedTitle = '', a, b) => `${providedTitle}: parsePackageNameV2('${a}') === '${b}'`

test(testParsePackageNameV2, 'lodash@npm:1.2.3', 'lodash')
test(testParsePackageNameV2, '@gitlab/ui@npm:1.2.3', '@gitlab/ui')
test(testParsePackageNameV2, '@gitlab/untamper-my-lockfile@workspace:.', '@gitlab/untamper-my-lockfile')
test(testParsePackageNameV2, 'resolve@patch:resolve@npm%3A1.20.0#~builtin<compat/resolve>::version=1.20.0&hash=07638b', 'resolve')

// why: We need to test aliases in v2
test(testParsePackageNameV2, 'jquery3@npm:jquery@3', 'jquery')
test(testParsePackageNameV2, 'my-react@npm:react@latest', 'react')
test(testParsePackageNameV2, 'my-react@patch:react@npm%3A2.2.2', 'react')

test('parsePackageNameV2 throws error if pattern does not match', async (t) => {
  t.throws(() => parsePackageNameV2('not-a-real-resolved-name'), { message: 'Found unepxected pattern for YarnV2 resolution while parsing package name: not-a-real-resolved-name' })
})

test('getPackageAlias', (t) => {
  t.is(getPackageAlias('lodash@npm:@gitlab/noop@1.0.0'), 'npm:@gitlab/noop@1.0.0')
  t.is(getPackageAlias('lodash@1.0.0'), null)
  t.is(getPackageAlias('lodash@https://example.org/archive.tar.gz'), null)
  t.is(getPackageAlias('lodash@file:/tmp/foo'), null)
  t.is(getPackageAlias('lodash@git+ssh://git@github.com:npm/cli.git#v1.0.27'), null)
  t.is(getPackageAlias('mocha@mochajs/mocha#4727d357ea'), null)
})
