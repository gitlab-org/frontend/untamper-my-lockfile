const test = require('ava')
const { parse: parseYaml } = require('yaml')

const { readV2DependenciesAsV1 } = require('./read-v2-dependencies-as-v1')

test('parses YarnV2 optional dependencies in YarnV1 format', t => {
  const yarnEntry = parseYaml(`
dependencies:
  anymatch: ~3.1.2
  braces: ~3.0.2
  fsevents: ~2.3.2
  glob-parent: ~5.1.2
  is-binary-path: ~2.1.0
  is-glob: ~4.0.1
  normalize-path: ~3.0.0
  readdirp: ~3.6.0
dependenciesMeta:
  fsevents:
    optional: true
  `)
  const actual = readV2DependenciesAsV1(yarnEntry)

  t.deepEqual(actual, {
    dependencies: {
      anymatch: '~3.1.2',
      braces: '~3.0.2',
      'glob-parent': '~5.1.2',
      'is-binary-path': '~2.1.0',
      'is-glob': '~4.0.1',
      'normalize-path': '~3.0.0',
      readdirp: '~3.6.0'
    },
    optionalDependencies: {
      fsevents: '~2.3.2'
    }
  })
})
