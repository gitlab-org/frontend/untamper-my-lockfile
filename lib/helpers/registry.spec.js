const test = require('ava')
const sinon = require('sinon')

const Registry = require('./registry')

const mockResult = { data: {} }

test('getPackageData: should call API once for the same package', async (t) => {
  const registry = new Registry()
  const mock = sinon.mock(registry)
  mock.expects('getPackageDataWithRetries').once().resolves(mockResult)

  const res = await Promise.all([
    registry.getPackageData('lodash'),
    registry.getPackageData('lodash')
  ])

  t.notThrows(() => mock.verify())
  t.deepEqual(res, [mockResult, mockResult])
})

test('getPackageDataWithRetries: should retry on errors', async (t) => {
  const registry = new Registry()
  const mock = sinon.mock(registry)
  mock.expects('pkg').exactly(2)
    .onCall(0).throws(new Error('this is an error'))
    .onCall(1).resolves(mockResult)

  const res = await registry.getPackageDataWithRetries('lodash')

  t.deepEqual(res, mockResult)
  t.notThrows(() => mock.verify())
})

test('getPackageDataWithRetries: throws after maximum of retries', async (t) => {
  const retries = 3
  const registry = new Registry()
  const error = new Error('lodash not found')
  const mock = sinon.mock(registry)

  mock.expects('pkg').exactly(retries).throws(error)

  await t.throwsAsync(
    registry.getPackageDataWithRetries('lodash', retries),
    { is: error }
  )

  t.notThrows(() => mock.verify())
})
