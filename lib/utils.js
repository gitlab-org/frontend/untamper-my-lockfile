const debug = require('debug')('untamper')

let inProgress = false
let progressCount = 0

const logProgress = (msg, done = false) => {
  if (process.stderr.isTTY) {
    if (inProgress) {
      progressCount += 1
      process.stderr.clearLine()
      process.stderr.cursorTo(0)
    }
    process.stderr.write(msg.replace('{count}', progressCount))
    if (done) {
      progressCount = 0
      process.stderr.write('\n')
    }
    inProgress = !done
  } else if (done) {
    progressCount = 0
    process.stderr.write(msg + '\n')
  }
}

const log = (...msg) => {
  console.warn(...msg)
}

const logger = {
  debug,
  logProgress,
  log
}

const createError = (message, file) => {
  return {
    type: 'ERROR',
    message,
    file
  }
}

/**
 * Mathematical complement of two objects. Compare the values of two objects with strict comparison.
 * In set theory it would have been something like:
 * object1 \ object2
 * As we are dealing with key, value pairs we are not just comparing the keys, but also the values themselves
 */
function objectComplement (object1 = {}, object2 = {}) {
  return Object.entries(object1).filter(([key, value]) => value !== object2[key])
}

module.exports = {
  createError,
  objectComplement,
  logger
}
