const fs = require('fs/promises')
const { basename } = require('path')

const { createYarnV1Strategy, createYarnV2Strategy } = require('./strategies')
const checkYarnEntries = require('./lock-files/check')
const { createError, logger } = require('./utils')
const { tryLoadingPackageJSON } = require('./helpers/load-package-json')

async function checkYarn (filePath, config = {}) {
  let yarnFile
  try {
    yarnFile = await fs.readFile(filePath, 'utf8')
  } catch (e) {
    return [createError(`Could not read lockfile: ${e.message}`, filePath)]
  }

  const isV2 = yarnFile.match(/^__metadata:$/m)
  const strategy = isV2 ? createYarnV2Strategy(config) : createYarnV1Strategy(config)
  const packageJson = isV2 ? null : await tryLoadingPackageJSON(filePath)

  try {
    yarnFile = strategy.parseYarnLockFile(yarnFile, filePath, packageJson)
  } catch (e) {
    return [createError(`Could not parse lockfile: ${e.message}`, filePath)]
  }

  if (yarnFile.type !== 'success') {
    return [createError('Parsed lock file has merge conflicts. Please fix them and try again.', filePath)]
  }

  return checkYarnEntries(yarnFile.entries, filePath, strategy.validate)
}

/**
 * Moved this logic into a separate function, so that once we support
 * more lockfiles, we can abstract the file type detection here.
 */
async function checkLockFile (filePath, config = {}) {
  const type = basename(filePath)

  switch (basename(filePath)) {
    case 'yarn.lock':
      return checkYarn(filePath, config)
    default:
      return [createError(`No support for lock files of type '${type}'`, filePath)]
  }
}

async function checkLockFiles (lockFiles, config = {}) {
  const filesToCheck = [lockFiles].flat().sort()

  logger.log(`Checking whether lockfile(s) ${filesToCheck.join(', ')} have been tampered with`)

  const results = []

  for (const filePath of filesToCheck) {
    results.push(...await checkLockFile(filePath, config))
  }

  return results.flat()
}

module.exports = checkLockFiles
