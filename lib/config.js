const fs = require('fs/promises')

const DEFAULT_CONFIG = {
  allowMissingChecksum: []
}

async function readConfig (configPath) {
  if (!configPath) {
    return DEFAULT_CONFIG
  }

  const configContent = await fs.readFile(configPath, 'utf-8')

  return {
    ...DEFAULT_CONFIG,
    ...JSON.parse(configContent)
  }
}

module.exports = {
  readConfig
}
