const asyncPool = require('tiny-async-pool')
const { logger, createError } = require('../utils')
const Registry = require('../helpers/registry')

async function checkPackage (yarnEntry, filePath, registry, validate) {
  const { version, packageName: name } = yarnEntry

  const logName = `${name}@${version}`
  let pkgData

  try {
    pkgData = await registry.getRemotePackageVersionInfo(name, version)
  } catch (e) {
    if (e instanceof Registry.VersionNotFoundError) {
      return createError(`${logName}: version '${version}' doesn't exist in registry`, filePath)
    } else {
      throw e
    }
  }

  return validate(yarnEntry, pkgData).map(x => createError(`${logName}: ${x}`, filePath))
}

async function checkYarnEntries (entries, filePath, validate) {
  const registry = new Registry()

  const processPackage = async (yarnEntry) => {
    const { allPatterns } = yarnEntry
    logger.debug('checking entry: %s', allPatterns.join(', '))
    logger.logProgress(`Checking ${filePath}: package {count} of ${entries.length}`, false)
    try {
      return await checkPackage(yarnEntry, filePath, registry, validate)
    } catch (e) {
      if (e instanceof Registry.MaxRetriesError) {
        throw e
      }
      return (createError(`Could not process entry for ${allPatterns.join(', ')}: ${e.message}`, filePath))
    }
  }

  let allMessages

  try {
    allMessages = (await asyncPool(10, entries, processPackage)).flat()
  } catch (e) {
    if (e instanceof Registry.MaxRetriesError) {
      logger.logProgress('', true)
      return [createError(e.message, filePath)]
    }
  }

  logger.logProgress(`Checking ${filePath}: package ${entries.length} of ${entries.length}`, true)

  return allMessages
}

module.exports = checkYarnEntries
