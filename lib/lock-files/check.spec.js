const { tmpdir } = require('os')
const { join } = require('path')
const test = require('ava')
const checkYarnEntries = require('./check')

const { setupMockRegistry, clearMockRegistry } = require('../../mocks/registry/setup-registry')
const { createYarnV1Strategy } = require('../strategies')

let yarnPath

// The mock registry contains all packages used in the tests below
// If you add a new test, please use one of the existing packages
// or refer to the ./mocks/registry README to add a new package to
// the mock registry.
test.before(() => {
  setupMockRegistry()
  yarnPath = join(tmpdir(), 'yarn.lock')
})

test.after(() => { clearMockRegistry() })

const strategy = createYarnV1Strategy()
const checkLockFile = async (string, packageJSON = { }) => {
  const { entries } = strategy.parseYarnLockFile(string, yarnPath, packageJSON)

  return checkYarnEntries(entries, yarnPath, strategy.validate)
}

test('should return empty array for valid lock file', async t => {
  const result = await checkLockFile(`
lodash@^4.17.20:
  version "4.17.20"
  resolved "https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52"
  integrity sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==
`)

  t.deepEqual(result, [])
})

test('should return empty array for valid lock file with bundled dependencies if the bundled dependency is listed in dependencies', async t => {
  const result = await checkLockFile(`
"@leipert/bundle-dependencies@1.0.0":
  version "1.0.0"
  resolved "https://registry.yarnpkg.com/@leipert/bundle-dependencies/-/bundle-dependencies-1.0.0.tgz#02508e8c53a66e1e998247b5f8018c9c55d70f54"
  integrity sha512-l8L4pegd5pEHJ9TlXOT4YB5wCk1zzFs47SsxIk/jp1l0kzcrHbiqmVR3KaBimGg/FCkAFL1XQ9pnD9s817NsqA==
  dependencies:
    ms "^2.1.3"

ms@^2.1.3:
  version "2.1.3"
  resolved "https://registry.yarnpkg.com/ms/-/ms-2.1.3.tgz#574c8138ce1d2b5861f0b44579dbadd60c6615b2"
  integrity sha512-6FlzubTLZG3J2a/NVCAleEhjzq5oxgHyaCU9yYXvcLsvoVaHJq/s5xXI6/XXP6tz7R9xAOtHnSO/tXtF3WRTlA==
`)

  t.deepEqual(result, [])
})

test('should return empty array for valid lock file with bundled dependencies if the bundled dependency is listed in yarn.lock', async t => {
  const result = await checkLockFile(`
"@leipert/bundle-dependencies@1.0.1":
  version "1.0.1"
  resolved "https://registry.yarnpkg.com/@leipert/bundle-dependencies/-/bundle-dependencies-1.0.1.tgz#d95693b641ca07711e64e16026f6efd9fd84e90d"
  integrity sha512-i+60+6xQbfpyS0PerlDC1bGrEQR5rSMq+0DdnZI/Cb/5mLWYY8SdjdXFJHM3+3i0zn38ihvo4LHnt6vxSWijJw==

ms@*:
  version "2.1.3"
  resolved "https://registry.yarnpkg.com/ms/-/ms-2.1.3.tgz#574c8138ce1d2b5861f0b44579dbadd60c6615b2"
  integrity sha512-6FlzubTLZG3J2a/NVCAleEhjzq5oxgHyaCU9yYXvcLsvoVaHJq/s5xXI6/XXP6tz7R9xAOtHnSO/tXtF3WRTlA==
`)

  t.deepEqual(result, [])
})

test('should return empty array for valid lock file with bundled dependencies if the bundled dependency is not listed in yarn.lock', async t => {
  // Removed optional dependency
  const result = await checkLockFile(`
"@leipert/bundle-dependencies@1.0.2":
  version "1.0.2"
  resolved "https://registry.yarnpkg.com/@leipert/bundle-dependencies/-/bundle-dependencies-1.0.2.tgz#82977b1cb2dc8d5483ae7bd47fb33041a5474127"
  integrity sha512-FDgzhqrbVRtQACds8ZZ28zZeg3gO5gT7zUsKFPbdLYlAA9OIk1Zi15q+3bzhcHFbSvtZI2Jvq0OGVjAJz/k9Vg==
`)

  t.deepEqual(result, [])
})

test('support for `npm:` alias', async t => {
  // Removed optional dependency
  const result = await checkLockFile(`
"example-package@npm:lodash@^4.17.20":
  version "4.17.20"
  resolved "https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52"
  integrity sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==
`)

  t.deepEqual(result, [])
})

test('should return error for typo-squatting URL', async t => {
  // Note that we replaced `lodash` with `nodash` in the URL
  const result = await checkLockFile(`
lodash@^4.17.20:
  version "4.17.20"
  resolved "https://registry.yarnpkg.com/nodash/-/nodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52"
  integrity sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==
`)

  t.deepEqual(result, [{
    file: yarnPath,
    message: 'lodash@4.17.20: resolved URL is https://registry.yarnpkg.com/nodash/-/nodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52, expected https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52',
    type: 'ERROR'
  }])
})

test('should return error for non-yarn registry URL', async t => {
  const result = await checkLockFile(`
lodash@^4.17.20:
  version "4.17.20"
  resolved "https://malicious-package.example.com/-/lodash-4.17.20.tgz"
  integrity sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==
`)

  t.deepEqual(result, [{
    file: yarnPath,
    message: 'lodash@4.17.20: resolved URL is https://malicious-package.example.com/-/lodash-4.17.20.tgz, expected https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52',
    type: 'ERROR'
  }])
})

test('should return error for sha512 integrity mismatch', async t => {
  const result = await checkLockFile(`
lodash@^4.17.20:
  version "4.17.20"
  resolved "https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52"
  integrity sha512-i-am-so-manipulated==
`)

  t.deepEqual(result, [{
    file: yarnPath,
    message: 'lodash@4.17.20: integrity hash is sha512-i-am-so-manipulated==, expected sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==',
    type: 'ERROR'
  }])
})

test('should return error for unknown version', async t => {
  const result = await checkLockFile(`
lodash@^9.9.9:
  version "9.9.9"
  resolved "https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52"
  integrity sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==
`)

  t.deepEqual(result, [{
    file: yarnPath,
    message: 'lodash@9.9.9: version \'9.9.9\' doesn\'t exist in registry',
    type: 'ERROR'
  }])
})

test('should return error for extraneous dependency', async t => {
  const result = await checkLockFile(`
lodash@^4.17.20:
  version "4.17.20"
  resolved "https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52"
  integrity sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==
  dependencies:
    ms "^2.1.3"

ms@^2.1.3:
  version "2.1.3"
  resolved "https://registry.yarnpkg.com/ms/-/ms-2.1.3.tgz#574c8138ce1d2b5861f0b44579dbadd60c6615b2"
  integrity sha512-6FlzubTLZG3J2a/NVCAleEhjzq5oxgHyaCU9yYXvcLsvoVaHJq/s5xXI6/XXP6tz7R9xAOtHnSO/tXtF3WRTlA==
`)

  t.deepEqual(result, [{
    file: yarnPath,
    message: 'lodash@4.17.20: Lockfile has \'ms@^2.1.3\' in \'dependencies\' while not listed at the registry.',
    type: 'ERROR'
  }])
})

test('should return error for extraneous optional dependency', async t => {
  const result = await checkLockFile(`
lodash@^4.17.20:
  version "4.17.20"
  resolved "https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52"
  integrity sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==
  optionalDependencies:
    ms "^2.1.3"

ms@^2.1.3:
  version "2.1.3"
  resolved "https://registry.yarnpkg.com/ms/-/ms-2.1.3.tgz#574c8138ce1d2b5861f0b44579dbadd60c6615b2"
  integrity sha512-6FlzubTLZG3J2a/NVCAleEhjzq5oxgHyaCU9yYXvcLsvoVaHJq/s5xXI6/XXP6tz7R9xAOtHnSO/tXtF3WRTlA==
`)

  t.deepEqual(result, [{
    file: yarnPath,
    message: 'lodash@4.17.20: Lockfile has \'ms@^2.1.3\' in \'optionalDependencies\' while not listed at the registry.',
    type: 'ERROR'
  }])
})

test('should return error for missing dependency', async t => {
  // Removed dependencies
  const result = await checkLockFile(`
keyv@3.1.0:
  version "3.1.0"
  resolved "https://registry.yarnpkg.com/keyv/-/keyv-3.1.0.tgz#ecc228486f69991e49e9476485a5be1e8fc5c4d9"
  integrity sha512-9ykJ/46SN/9KPM/sichzQ7OvXyGDYKGTaDlKMGCAlg2UK8KRy4jb0d8sFc+0Tt0YYnThq8X2RZgCg74RPxgcVA==
`)

  t.deepEqual(result, [{
    file: yarnPath,
    message: 'keyv@3.1.0: Lockfile is missing \'json-buffer@3.0.0\' in \'dependencies\'.',
    type: 'ERROR'
  }])
})

test('should return error for missing optional dependency', async t => {
  // Removed optional dependency
  const result = await checkLockFile(`
jsonfile@4.0.0:
  version "4.0.0"
  resolved "https://registry.yarnpkg.com/jsonfile/-/jsonfile-4.0.0.tgz#8771aae0799b64076b76640fca058f9c10e33ecb"
  integrity sha1-h3Gq4HmbZAdrdmQPygWPnBDjPss=
`)

  t.deepEqual(result, [{
    file: yarnPath,
    message: 'jsonfile@4.0.0: Lockfile is missing \'graceful-fs@^4.1.6\' in \'optionalDependencies\'.',
    type: 'ERROR'
  }])
})

test('should deduplicate errors if entry has multiple keys', async t => {
  // Note that we replaced `lodash` with `nodash` in the URL
  const result = await checkLockFile(`
lodash@4.17.20, "zopfli@npm:lodash@^4.17.20":
  version "4.17.20"
  resolved "https://registry.yarnpkg.com/nodash/-/nodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52"
  integrity sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==
  
ms@*:
  version "2.1.3"
  resolved "https://registry.yarnpkg.com/ms/-/ms-2.1.3.tgz#574c8138ce1d2b5861f0b44579dbadd60c6615b2"
  integrity sha512-6FlzubTLZG3J2a/NVCAleEhjzq5oxgHyaCU9yYXvcLsvoVaHJq/s5xXI6/XXP6tz7R9xAOtHnSO/tXtF3WRTlA==
`)

  t.deepEqual(result, [{
    file: yarnPath,
    message: 'lodash@4.17.20: resolved URL is https://registry.yarnpkg.com/nodash/-/nodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52, expected https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52',
    type: 'ERROR'
  }])
})

test('should not deduplicate errors if multiple consecutive entries are the exact same', async t => {
  // Note that we replaced `lodash` with `nodash` in the URL
  const result = await checkLockFile(`
lodash@4.17.20:
  version "4.17.20"
  resolved "https://registry.yarnpkg.com/nodash/-/nodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52"
  integrity sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==

"zopfli@npm:lodash@^4.17.20":
  version "4.17.20"
  resolved "https://registry.yarnpkg.com/nodash/-/nodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52"
  integrity sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==
`)

  t.deepEqual(result, [{
    file: yarnPath,
    message: 'lodash@4.17.20: resolved URL is https://registry.yarnpkg.com/nodash/-/nodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52, expected https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52',
    type: 'ERROR'
  }, {
    file: yarnPath,
    message: 'lodash@4.17.20: resolved URL is https://registry.yarnpkg.com/nodash/-/nodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52, expected https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52',
    type: 'ERROR'
  }
  ])
})

test('should not deduplicate errors if entry has multiple entries with different errors', async t => {
  // Note that we replaced `lodash` with `nodash` in the first URL
  // Note that we replaced `lodash` with `modash` in the second URL
  const result = await checkLockFile(`
lodash@^4.17.20:
  version "4.17.20"
  resolved "https://registry.yarnpkg.com/nodash/-/nodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52"
  integrity sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==
  
"zopfli@npm:lodash@^4.17.20":
  version "4.17.20"
  resolved "https://registry.yarnpkg.com/modash/-/modash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52"
  integrity sha512-PlhdFcillOINfeV7Ni6oF1TAEayyZBoZ8bcshTHqOYJYlrqzRK5hagpagky5o4HfCzzd1TRkXPMFq6cKk9rGmA==
`)

  t.deepEqual(result, [{
    file: yarnPath,
    message: 'lodash@4.17.20: resolved URL is https://registry.yarnpkg.com/nodash/-/nodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52, expected https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52',
    type: 'ERROR'
  },
  {
    file: yarnPath,
    message: 'lodash@4.17.20: resolved URL is https://registry.yarnpkg.com/modash/-/modash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52, expected https://registry.yarnpkg.com/lodash/-/lodash-4.17.20.tgz#b44a9b6297bcb698f1c51a3545a2b3b368d59c52',
    type: 'ERROR'
  }
  ])
})

test('should raise errors when no dependency resolution matches', async t => {
  const result = await checkLockFile(`
jackspeak@^2.3.5, "jackspeak@npm:@gitlab/noop@1.0.0":
  version "1.0.0"
  resolved "https://registry.yarnpkg.com/@gitlab/noop/-/noop-1.0.0.tgz#b1ecb8ae6b2abf9b2e28927e4fbb05b7a1b2704b"
  integrity sha512-nOltttik5o2BjBo8LnyeTFzHoLpMY/XcCVOC+lm9ZwU+ivEam8wafacMF0KTbRn1KVrIoHYdo70QnqS+vJiOVw==
`, { resolutions: { } })

  t.deepEqual(result, [
    {
      file: yarnPath,
      message: 'jackspeak@1.0.0: version \'1.0.0\' doesn\'t exist in registry',
      type: 'ERROR'
    }
  ])
})

test('should not raise errors when using dependency resolutions', async t => {
  const result = await checkLockFile(`
jackspeak@^2.3.5, "jackspeak@npm:@gitlab/noop@1.0.0":
  version "1.0.0"
  resolved "https://registry.yarnpkg.com/@gitlab/noop/-/noop-1.0.0.tgz#b1ecb8ae6b2abf9b2e28927e4fbb05b7a1b2704b"
  integrity sha512-nOltttik5o2BjBo8LnyeTFzHoLpMY/XcCVOC+lm9ZwU+ivEam8wafacMF0KTbRn1KVrIoHYdo70QnqS+vJiOVw==
`, { resolutions: { '**/glob/jackspeak': 'npm:@gitlab/noop@1.0.0' } })

  t.deepEqual(result, [])
})
