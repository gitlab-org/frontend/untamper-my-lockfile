const { parse: parseYarnLockFile } = require('@yarnpkg/lockfile')

const { parsePackageName, getPackageAlias } = require('../helpers/parse-package-name')
const { validateDependencyEntries, validateIntegrity, validateResolutionPath } = require('./v1-validators')

/**
 * Deduplicate a parsed yarn object.
 *
 * For de-duplicated yarn entries, the parser adds actually two entries to the object:
 * ```
 * lodash@4.17.20, lodash@^4.17.20:
 *   version "4.17.20"
 *   resolved "<url>"
 *   integrity "<hash>"
 * ```
 *
 * The example above will present as a hash, where <obj> is identical for both entries.
 *
 * ```
 * {
 *   "lodash@4.17.20": <obj>,
 *   "lodash@^4.17.20": <obj>
 * }
 * ```
 *
 * This function returns an array with the unique Set of entries.
 * Each entry has the patterns pointing to it added.
 *
 * So in our example above the entry for both lodash patterns would
 * look like this:
 *
 * ```
 * {
 *     version: '4.17.20',
 *     resolved: '<url>',
 *     integrity: '<integrity-hash>',
 *     allPatterns: [ 'lodash@4.17.20', 'lodash@^4.17.20' ]
 * }
 * ```
 */
function deduplicatedYarnEntries (yarnObject, resolutions = {}) {
  const acc = new Set()

  /**
   * Selective resolutions
   * https://classic.yarnpkg.com/lang/en/docs/selective-version-resolutions/
   * are a map from glob to different package (version)
   *
   * ```
   * "react": "^18.0.0", // forcing a different version of react
   * "d2\/left-pad": "npm:lodash@1.0.0", // replacing left-pad with lodash
   * "**\/glob\/left-pad": "npm:foobar@1.2.3", // replacing left-pad with foobar
   * ```
   *
   * In the example above the aliasResolutionToPackage would look like:
   *
   * ```
   * {
   *   "npm:lodash@1.0.0": Set("left-pad"),
   *   "npm:foobar@1.2.3": Set("left-pad"),
   * }
   * ```
   *
   * We need this, because our yarn.lock might look like:
   *
   * ```
   * left-pad@2.0.0, "left-pad@npm:lodash@1.0.0":
   *   version "1.0.0"
   *   resolved "<url to LODASH!>"
   *   integrity "<hash>"
   *
   * left-pad@3.0.0, "left-pad@npm:foobar@1.2.3":
   *   version "1.2.3"
   *   resolved "<url to FOOBAR!>"
   *   integrity "<hash>"
   * ```
   *
   * In both those cases `untamper-my-lockfile` would fail, because it'd expect the URL
   * to resolve to an actual "left-pad" package rather than the forced resolutions of
   * lodash / foobar respectively.
   *
   */
  const aliasResolutionToPackage = {}
  for (const [glob, resolution] of Object.entries(resolutions)) {
    if (resolution.startsWith('npm:')) {
      const globSplit = glob.split('/')
      aliasResolutionToPackage[resolution] ||= new Set()
      aliasResolutionToPackage[resolution].add(globSplit[globSplit.length - 1])
    }
  }

  for (const [pattern, yarnEntry] of Object.entries(yarnObject)) {
    yarnEntry.allPatterns ||= []
    yarnEntry.allPatterns.push(pattern)

    const packageName = parsePackageName(pattern)
    yarnEntry.packageName ||= packageName
    if (yarnEntry.packageName !== packageName) {
      const alias = getPackageAlias(pattern)
      if (aliasResolutionToPackage[alias] && aliasResolutionToPackage[alias].has(yarnEntry.packageName)) {
        yarnEntry.packageName = packageName
      }
    }
    acc.add(yarnEntry)
  }

  return [...acc]
}

function createYarnV1Strategy () {
  return {
    parseYarnLockFile (yarnFile, filePath, packageJSON) {
      const { type, object } = parseYarnLockFile(yarnFile, filePath)
      const { resolutions = {} } = (packageJSON || {})

      if (type === 'success') {
        return {
          type,
          entries: deduplicatedYarnEntries(object, resolutions)
        }
      }

      return { type }
    },
    validate (yarnEntry, pkgData) {
      return [
        ...validateDependencyEntries(yarnEntry, pkgData),
        validateIntegrity(yarnEntry, pkgData),
        validateResolutionPath(yarnEntry, pkgData)
      ].filter(x => !x.isValid).map(x => x.message)
    }
  }
}

module.exports = {
  createYarnV1Strategy
}
