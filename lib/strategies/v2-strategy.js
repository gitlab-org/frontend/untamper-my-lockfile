const { parse: parseYaml } = require('yaml')
const { parsePackageNameV2 } = require('../helpers/parse-package-name')
const { readV2DependenciesAsV1 } = require('../helpers/read-v2-dependencies-as-v1')
const { validateChecksumExists, validateResolutionPath, validateDependencyEntries } = require('./v2-validators')

function isWorkspaceEntry (yarnEntry) {
  const { linkType, resolution } = yarnEntry

  return linkType === 'soft' && resolution.includes('@workspace:')
}

function createYarnV2Strategy (config) {
  return {
    parseYarnLockFile (text) {
      // why: We use the "failsafe" schema so that numbers will be treated as Strings
      //      - https://eemeli.org/yaml/#schema-options
      const { __metadata, ...data } = parseYaml(text, { schema: 'failsafe' })

      const entries = Object.entries(data)
        // what: We can ignore workspace: entries.
        // why:  `yarn install --immutable` is enough there.
        .filter(([key, value]) => !isWorkspaceEntry(value))
        // what: This transforms the yarn2 "lodash@111, lodash@432" keys intro separate entries
        // why: Whis way we can conform to how the original yarn 1 processing works
        .map(([key, value]) => {
          const allPatterns = key.split(', ')
          const packageName = parsePackageNameV2(allPatterns[0])
          const v1StyleDependencies = readV2DependenciesAsV1(value)

          return {
            ...value,
            ...v1StyleDependencies,
            allPatterns,
            packageName
          }
        })

      // what: Conform to parseYarnV1 result type
      return {
        type: 'success',
        entries
      }
    },
    validate (yarnEntry, pkgData) {
      return [
        ...validateDependencyEntries(yarnEntry, pkgData),
        validateChecksumExists(yarnEntry, config),
        validateResolutionPath(yarnEntry, pkgData)
      ].filter(x => !x.isValid).map(x => x.message)
    }
  }
}

module.exports = {
  createYarnV2Strategy
}
