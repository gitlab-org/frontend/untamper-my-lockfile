const test = require('ava')
const { validateResolutionPath } = require('./v2-validators')

function testValidateResolutionPath (t, { input, output }) {
  t.deepEqual(validateResolutionPath(input), output)
}
testValidateResolutionPath.title = (providedTitle = '') => `validateResolutionPath: ${providedTitle}`

const TEST_RESOLVE_NO_HASH = 'fsevents@patch:fsevents@npm%3A2.3.2#~builtin<compat/fsevents>::version=2.3.2'
const TEST_RESOLVE = `${TEST_RESOLVE_NO_HASH}&hash=18f3a7`
const TEST_VERSION = '2.3.2'
const TEST_YARN_ENTRY = {
  version: TEST_VERSION,
  resolution: TEST_RESOLVE,
  allPatterns: [
    'fsevents@patch:fsevents@^2.3.2#~builtin<compat/fsevents>',
    'fsevents@patch:fsevents@~2.3.2#~builtin<compat/fsevents>'
  ],
  packageName: 'fsevents'
}

test('works with patch resolution', testValidateResolutionPath, {
  input: TEST_YARN_ENTRY,
  output: { isValid: true }
})

test('is invalid if patch resolution is missing hash', testValidateResolutionPath, {
  input: {
    ...TEST_YARN_ENTRY,
    resolution: TEST_RESOLVE_NO_HASH
  },
  output: { isValid: false, message: `resolution is ${TEST_RESOLVE_NO_HASH}, expected to match patch resolution pattern (version: ${TEST_VERSION})` }
})

test('is invalid if allPatterns are not patches', testValidateResolutionPath, {
  input: {
    ...TEST_YARN_ENTRY,
    allPatterns: [
      ...TEST_YARN_ENTRY.allPatterns,
      'fsevents@npm:2.3.2'
    ]
  },
  output: { isValid: false, message: `resolution is ${TEST_YARN_ENTRY.resolution}, expected to match npm resolution pattern (version: ${TEST_VERSION})` }
})

test('works with npm resolution', testValidateResolutionPath, {
  input: {
    ...TEST_YARN_ENTRY,
    resolution: 'fsevents@npm:2.3.2',
    allPatterns: [
      'fsevents@npm:latest',
      'fsevents@npm:2^'
    ]
  },
  output: { isValid: true }
})

test('is invalid if npm resolution does not match exactly', testValidateResolutionPath, {
  input: {
    ...TEST_YARN_ENTRY,
    resolution: 'fsevents@npm:2.3.2?extra',
    allPatterns: [
      'fsevents@npm:latest',
      'fsevents@npm:2^'
    ]
  },
  output: {
    isValid: false,
    message: `resolution is fsevents@npm:2.3.2?extra, expected to match npm resolution pattern (version: ${TEST_VERSION})`
  }
})
