const { omit, escapeRegExp } = require('lodash')
const v1Validators = require('./v1-validators')

const PKG_NODE_GYP = 'node-gyp'

function isAnyDependencyOf (dependencyName, pkgData) {
  return ['dependencies', 'devDependencies', 'optionalDependencies'].some(type => pkgData[type]?.[dependencyName])
}

/**
 * Validate checksum of yarn2 entry
 *
 * In Yarn2, the checksum is the SHA-512 of the actual .zip that will be written to disk.
 * There's no way to verify this against a npm registry and yarn handles verification
 * pretty nicely. As long as the "resolution" path is valid, we should be good.
 */
function validateChecksumExists (yarnEntry, config) {
  const { checksum, packageName } = yarnEntry

  // If we have a checksum or are a workspace entry, we're good.
  if (!checksum && !config.allowMissingChecksum.includes(packageName)) {
    return {
      isValid: false,
      message: 'no checksum found'
    }
  }

  return { isValid: true }
}

function validateDependencyEntries (yarnEntry, pkgData) {
  // what: Yarn2 does something weird where it automatically adds node-gyp@latest as a
  //       dependency if it's found being used in a script... We need to add an exception
  //       for this here.
  //       Since we don't have access to the `scripts`, we'll just look to see if it's referenced
  //       in devDependencies
  //       - https://sourcegraph.com/github.com/yarnpkg/berry@@yarnpkg/shell/3.2.4/-/blob/packages/plugin-npm/sources/NpmSemverResolver.ts?L139
  if (yarnEntry.dependencies[PKG_NODE_GYP] === 'latest' && isAnyDependencyOf(PKG_NODE_GYP, pkgData)) {
    const cleanedYarnEntry = {
      ...yarnEntry,
      dependencies: omit(yarnEntry.dependencies, PKG_NODE_GYP)
    }

    return v1Validators.validateDependencyEntries(cleanedYarnEntry, pkgData)
  }

  return v1Validators.validateDependencyEntries(yarnEntry, pkgData)
}

function getExpectedResolutionRegex (yarnEntry) {
  const { packageName, version, allPatterns } = yarnEntry

  const isPatchPattern = allPatterns.every(pattern => pattern.startsWith(`${packageName}@patch:`))

  if (isPatchPattern) {
    return {
      description: 'patch resolution pattern',
      regex: new RegExp(`^${escapeRegExp(packageName)}@patch:${escapeRegExp(packageName)}@npm%3A${escapeRegExp(version)}#~.*hash=[0-9a-z]+`)
    }
  }

  return {
    description: 'npm resolution pattern',
    regex: new RegExp(`^${escapeRegExp(packageName)}@npm:${escapeRegExp(version)}$`)
  }
}

/**
 * Validate resolution path of yarn2 entry
 */
function validateResolutionPath (yarnEntry) {
  const { resolution, version } = yarnEntry

  const { regex, description } = getExpectedResolutionRegex(yarnEntry)

  if (!resolution.match(regex)) {
    return {
      isValid: false,
      message: `resolution is ${resolution}, expected to match ${description} (version: ${version})`
    }
  }

  return {
    isValid: true
  }
}

module.exports = {
  validateChecksumExists,
  validateDependencyEntries,
  validateResolutionPath
}
