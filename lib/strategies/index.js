module.exports = {
  ...require('./v1-strategy'),
  ...require('./v2-strategy')
}
