const { objectComplement } = require('../utils')

function toSha1IntegrityString (shasum) {
  return 'sha1-' + Buffer.from(shasum, 'hex').toString('base64')
}

function validateDependencyEntries (yarnEntry, versionPackageData) {
  const { bundledDependencies } = versionPackageData

  return ['dependencies', 'optionalDependencies']
    .flatMap(type => {
      const extraDepErrors = objectComplement(yarnEntry[type], versionPackageData[type])
        .map(([packageName, version]) => `Lockfile has '${packageName}@${version}' in '${type}' while not listed at the registry.`)

      const missingDepErrors = objectComplement(versionPackageData[type], yarnEntry[type])
        .flatMap(([packageName, version]) => {
          /**
           * There are three ways bundled dependencies can interact with a yarn.lock
           * a.) package.json { dependencies: { example : '1.0.0'}, bundleDependencies: ['example']}
           *    => In this case the bundled dependency _will_ appear in the yarn.lock dependencies
           * b.) package.json { dependencies: { example : '1.0.0'}, bundleDependencies: ['sub_dependency_of_example']}
           *    1. => In this case the bundled dependency _might_ appear in the yarn.lock dependencies as sub_dependency_of_example: '*'
           *    2. => In this case the bundled dependency _might_ not appear in the yarn.lock dependencies
           */
          if (bundledDependencies.includes(packageName)) {
            return []
          }
          return `Lockfile is missing '${packageName}@${version}' in '${type}'.`
        })

      return extraDepErrors.concat(missingDepErrors).map(message => ({ isValid: false, message }))
    })
}

/**
 * Validate integrity of yarn1 entry
 */
function validateIntegrity (yarnEntry, pkgData) {
  const { integrity } = yarnEntry
  const { dist } = pkgData

  // expectedIntegrity is a SHA512 hash of the package's tarball. If the SHA512 is not
  // present, it will be a SHA1 hash. In both cases, the hash is base64 encoded.
  //
  // Example:
  // sha512-iRDPJKUPVEND7dHPO8rkbOnPpyDygcDFtWjpeWNCgy8WP2rXcxXL8TskReQl6OrB2G7+UJrags1q15Fudc7G6w==
  // sha1-s2nW+128E+7PUk+RsHD+7cNXzzQ=
  const expectedIntegrity = integrity.startsWith('sha512-') ? dist.integrity : toSha1IntegrityString(dist.shasum)

  if (integrity !== expectedIntegrity) {
    return {
      isValid: false,
      message: `integrity hash is ${integrity}, expected ${expectedIntegrity}`
    }
  }
  return {
    isValid: true
  }
};

/**
 * Validate resolution path of yarn1 entry
 */
function validateResolutionPath (yarnEntry, pkgData) {
  const { resolved } = yarnEntry
  const { dist } = pkgData

  // 'yarn info' resolves packages to registry.npmjs.org and doesn't respect --registry flag
  // change the registry manually to yarn's registry.
  const expectedResolved = dist.tarball.replace(
    'https://registry.npmjs.org',
    'https://registry.yarnpkg.com') +
    '#' + dist.shasum

  if (expectedResolved !== resolved) {
    return {
      isValid: false,
      message: `resolved URL is ${resolved}, expected ${expectedResolved}`
    }
  }

  return {
    isValid: true
  }
}

module.exports = {
  validateDependencyEntries,
  validateIntegrity,
  validateResolutionPath
}
