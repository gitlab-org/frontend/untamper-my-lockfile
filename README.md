# untamper-my-lockfile

A tool for checking if `yarn.lock` was tampered with.

**IMPORTANT**: Do not rename the CI template `.gitlab-ci-template.yml` or change the visibility of this project. Doing so would break the CI pipelines of projects that "include:" the CI template.

## Why

The content of `yarn.lock` can be manipulated to resolve a declared dependency to an alternative, attacker-controlled dependencies.
This tool prevents such an attack scenario by checking that dependencies in `yarn.lock` are resolved correctly.

## Usage

### GitLab CI

Please see our instructions in [./templates](./templates/).

### Locally

The safest variant is running this tool in docker:

```bash
docker run --volume "$(pwd)/yarn.lock:/tmp/yarn.lock" --rm \
  registry.gitlab.com/gitlab-org/frontend/untamper-my-lockfile:main \
  untamper-my-lockfile --lockfile /tmp/yarn.lock
```

Otherwise you need node@10 or newer.

```bash
yarn global add '@gitlab/untamper-my-lockfile'
untamper-my-lockfile --lockfile yarn.lock
```

## How it works

In [Yarn 1](https://classic.yarnpkg.com/lang/en/), each entry in `yarn.lock` is of the form:

```
dependency@semver:
  version: x.y.z
  resolved "https://registry.yarnpkg.com/..."
  integrity "hash"
```

In [Yarn 2+](https://yarnpkg.com/), each entry is in YAML form:

```
"dependency@npm:semver, dependecy@npm:~semver2":
  version: x.y.z
  resolution: "dependency@npm:x.y.z"
  checksum: abcdef123...
```

This tool iterates over each entry in the lockfile and does:

1. Retrieve the package (`dependency`) information information from https://registry.npmjs.org
1. If the version `x.y.z` doesn't exist on the registry => Warning
1. If the `resolved` URL or `integrity` hashes do not match against what is published on the registry => Error
