# Usage with CI template

We provide two kind of CI templates. Both of them run on commits in your default branch (e.g. `main`, `master`) and depending on which template you choose:

- `branch_pipelines.yml`: on every branch commit that changes `yarn.lock`
- `merge_request_pipelines.yml`: on every [Merge request](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html) that changes `yarn.lock`.

Additionally they both run on on branches / MRs if the branch is named `add-untamper-my-lockfile` in order to allow for runs during onboarding.

You can also define own jobs, utilizing our untamper-my-lockfile docker image.

## Branch Pipelines

```yaml
include:
  - project: 'gitlab-org/frontend/untamper-my-lockfile'
    file: 'templates/branch_pipelines.yml'
```

## Merge Request Pipelines

```yaml
include:
  - project: 'gitlab-org/frontend/untamper-my-lockfile'
    file: 'templates/merge_request_pipelines.yml'
```

## Custom job definition

```yaml
untamper-my-lockfile:
  image: registry.gitlab.com/gitlab-org/frontend/untamper-my-lockfile:main
  needs: []
  stage: test
  script:
    - untamper-my-lockfile --lockfile yarn.lock
  rules:
    - changes:
        - yarn.lock
```
