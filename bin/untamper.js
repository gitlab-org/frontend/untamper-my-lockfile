#!/usr/bin/env node
'use strict'

const argv = require('minimist')(process.argv.slice(2))
const chalk = require('chalk')

const checkLockFiles = require('../lib/check-lock-files')
const { readConfig } = require('../lib/config')
const { logger } = require('../lib/utils')

async function check () {
  if (argv.v || argv.version) {
    console.log(`@gitlab/untamper-my-lockfile – v${require('../package.json').version}`)
    process.exit(0)
  }

  const filename = argv.l || argv.lockfile || 'yarn.lock'
  const configPath = argv.config

  const config = await readConfig(configPath)
  const messages = await checkLockFiles(filename, config)

  if (messages.length === 0) {
    logger.log(chalk.green('[✓]') + ' Found no signs that the lockfile have been tampered with')
  } else {
    logger.log(chalk.red('[X]') + ' Found problems with the lockfile(s):')
    for (const a of messages) {
      const color = a.type === 'ERROR' ? chalk.red : chalk.yellow
      logger.log(`- ${color(a.type)} ${a.file}: ${a.message}`)
    }
    process.exit(1)
  }
}

check().catch((err) => {
  logger.log('Unexpected error occurred: ')
  logger.log(err)
  process.exit(127)
})
