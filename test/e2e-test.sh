#!/bin/bash

cmd=""
if [ "${CI:false}" = "true" ]; then
  cmd="untamper-my-lockfile"
  if ! command -v "$cmd" &> /dev/null; then
      echo "$cmd could not be found"
      exit 1
  fi
else
  SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
  cmd="$SCRIPT_DIR/../bin/untamper.js"
  echo "Seemingly not running in CI, so falling back on $cmd"
fi

function check_file() {
  local file="$1"
  local expected_code="$2"
  local snapshot
  local exit_code
  snapshot="$file.snapshot.txt"
  local result
  result="$file.result.txt"

  rm -f "$result"

  echo "Testing $file"
  "$cmd" --config ./test/fixtures/config/.untamper-my-lockfile.valid.json --lockfile "$1" &> "$result"
  exit_code="$?"

  if ! cmp -s "$snapshot" "$result"; then
    echo -e "\tOutput between $snapshot and $result differs..."
    diff "$snapshot" "$result"
    exit 1
  fi

  if [ "$exit_code" -ne "$expected_code" ]; then
    echo -e "\tTEST FAILED. Expected $expected_code, retrieved $exit_code"
    exit 1
  fi

  echo -e "\tTest succeeded..."
}

check_file ./test/fixtures/unsupported/package-lock.json 1
check_file ./test/fixtures/v1/tampered/yarn.lock 1
check_file ./test/fixtures/v1/untampered/yarn.lock 0
check_file ./test/fixtures/v1/resolution-v1/yarn.lock 0
check_file ./test/fixtures/v1/resolution-v2/yarn.lock 0
check_file ./test/fixtures/v2/untampered-web-ide-snapshot/yarn.lock 0
check_file ./test/fixtures/v2/tampered-web-ide-snapshot/yarn.lock 1
check_file ./test/fixtures/v2/untampered-with-aliases/yarn.lock 0

echo 'TESTS SUCCEEDED'
exit 0
