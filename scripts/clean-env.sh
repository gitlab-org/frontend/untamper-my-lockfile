#!/bin/sh
set -eo

echo "Executing $@ with a (mostly) clean env to prevent env variable leaks"

env -i \
  PATH="$PATH" \
  CI="$CI" \
  ASDF_DATA_DIR="$ASDF_DATA_DIR" \
  ASDF_DIR="$ASDF_DIR" \
  "$@"
